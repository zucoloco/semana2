#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 13:47:01 2020

@author: gustavo
"""


import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter



T = np.zeros((100, 100)) 
feq = np.zeros((100, 100, 4))
omega = 1.0
f = np.zeros((100, 100, 4))
f_linha = np.zeros((100, 100, 4)) #para atualizar o vetor na propagação sem sobrepor valores
u = 0.1
v = 0.2

for t in range(400):
    for x in range(0,100):
        for y in range(0,100):
            T[x, y] = sum(f[x, y, :])
            feq[x, y, 0] = T[x, y] * 0.25 * (1 + 2 * u)
            feq[x, y, 1] = T[x, y] * 0.25 * (1 - 2 * u)
            feq[x, y, 2] = T[x, y] * 0.25 * (1 + 2 * v)
            feq[x, y, 3] = T[x, y] * 0.25 * (1 - 2 * v)
            
            for k in range(0, 4):
                f[x, y, k] = f[x, y, k]*(1 - omega) + omega * feq[x, y, k] #colisão
        
    for x in range(1, 99): #propagação
        for y in range(1, 99):
            f_linha[x, y, 0] = f[x+1, y, 0]
            f_linha[x, y, 1] = f[x-1, y, 1]
            f_linha[x, y, 2] = f[x, y+1, 2]
            f_linha[x, y, 3] = f[x, y-1, 3]
            
    for p in range(0,100): #condições de contorno 
        f_linha[0, p, 0] = 0.5 - f_linha[0, p, 1] #left boundary is kept at T = 1.0
        f_linha[0, p, 2] = 0.5 - f_linha[0, 1, 3] #left boundary is kept at T = 1.0
        
        f_linha[99, p, : ] = 0.0 #right boundary is kept at T = 0.0
        f_linha[p, 99, : ] = 0.0 #upper boundary is kept at T = 0.0
        f_linha[p, 0, : ] = f_linha[p, 1, : ] #bottom boundary is adiabatic 


    tmp = f
    f = f_linha
    f_linha = tmp 
    

X = np.arange(0, 100, 1)
Y = np.arange(0, 100, 1)
X, Y = np.meshgrid(X, Y)
    
fig = plt.figure()
ax = fig.gca(projection='3d')

surf = ax.plot_surface(X, Y, T, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

ax.set_zlim(-0.1, 1.01)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()